#!/usr/bin/perl

package MPVVolNorm::Server;
use Moo;
extends 'Web::Simple::Application';

use DBI qw(:sql_types);
use File::stat qw();
use IPC::Run3;
use JSON qw(encode_json);
use XML::TreePP;


has dbh => (
	is => 'ro',
	lazy => 1,
	builder => '_build_dbh',
);

sub default_config {
	(
		volume_db_dsn => 'dbi:SQLite:dbname=volume_db.sqlite',
		volume_db_user => '',
		volume_db_pass => '',

		media_path => '/srv/videos',
	);
}

sub dispatch_request {
	my $self = shift;

	(
		'/volume/fast/**.*' => sub { $_[0]->get_volume($_[1], 0) },
		'/volume/slow/**.*' => sub { $_[0]->get_volume($_[1], 1) },
		'/' => sub { $_[0]->return_about },
	);
}

sub return_about {
	[ 200, [ 'Content-type', 'text/plain; charset=us-ascii' ], [<<BODY] ];
This is an mpv-volnorm-server API server. It just returns volume level
information. It's only intented to be used by the mpv lua client script.
For details, see:

   https://gitlab.com/derobert/mpv-volnorm
BODY
}

sub get_volume {
	my ($self, $file, $compute) = @_;

	# avoid path traversal
	$file =~ m!/\.\.$! and die "invalid file";
	$file =~ m!/\.\./! and die "invalid file";
	$file =~ m!^\.\./! and die "invalid file";
	$file =~ m!^\.\.$! and die "invalid file";

	my $fullfile = $self->config->{media_path} . '/' . $file;
	-f $fullfile or die "invalid file";
	my $stat = File::stat::populate(CORE::stat(_)); # vomit

	my $res = $self->_get_volume_db($fullfile, $stat);
	if (!$res && $compute) {
		$res = $self->_get_volume_bs($fullfile);
		$res or die "bug: _get_volume_bs returned undef";
		$self->_save_volume_db($res, $fullfile, $stat);
	}

	return $res
		? [ 200, ['Content-type', 'application/json; charset=utf-8'], [encode_json($res)]]
		: [204, [], []];
}

sub _get_volume_db {
	my ($self, $fullfile, $stat) = @_;
	my $res;

	# we do not check st_dev, as it can change on reboot.
	my $sth = $self->dbh->prepare_cached(<<QUERY);
SELECT st_ino, st_mtime, st_size, vol_lufs, vol_err
  FROM volumes
 WHERE path = ?
QUERY
	$sth->bind_param(1, $fullfile, SQL_VARCHAR);
	$sth->execute;
	my $row = $sth->fetchrow_hashref;
	if ($row
		&& $row->{st_ino} == $stat->ino
		&& $row->{st_mtime} == $stat->mtime
		&& $row->{st_size} == $stat->size)
	{
		$res = {lufs => $row->{vol_lufs}, error => $row->{vol_err}};
	}    # else $res remains undef

	$sth->finish;
	return $res;
}

sub _save_volume_db {
	my ($self, $res, $fullfile, $stat) = @_;

	my $sth = $self->dbh->prepare(<<QUERY);
INSERT OR REPLACE INTO volumes (
  path, st_dev, st_ino, st_mtime, st_size, vol_lufs, vol_err
) VALUES (
  ?,    ?,      ?,      ?,        ?,       ?,        ?
)
QUERY

	$sth->bind_param(1, $fullfile, SQL_VARCHAR);
	$sth->bind_param(2, $stat->dev, SQL_INTEGER);
	$sth->bind_param(3, $stat->ino, SQL_INTEGER);
	$sth->bind_param(4, $stat->mtime, SQL_INTEGER);
	$sth->bind_param(5, $stat->size, SQL_INTEGER);
	$sth->bind_param(6, $res->{lufs}, SQL_DOUBLE);
	$sth->bind_param(7, $res->{error}, SQL_VARCHAR);
	$sth->execute;
	$sth->finish;

	return;
}

sub _get_volume_bs {
	my ($self, $fullfile) = @_;

	my $tpp = XML::TreePP->new(
		force_array => [qw(track)],
		force_hash  => [qw(album)]);

	my ($stdout, $stderr);
	run3 [ 'bs1770gain', '--xml', '-i', $fullfile ], \undef, \$stdout, \$stderr;
	$? and return { error => "bs1770gain exit code ${ \($? >> 8) }" };

	my $tree = $tpp->parse($stdout)
		or return { error => "XML parse failed" };

	exists $tree->{bs1770gain}{album}{track}[0]{integrated}{-lufs}
		or return { error => "Could not find integrated LUFS in XML" };

	return {
		lufs => $tree->{bs1770gain}{album}{track}[0]{integrated}{-lufs}
	};
}

sub _build_dbh {
	my $self = shift;
	my $dbh = DBI->connect(
		$self->config->{volume_db_dsn},
		$self->config->{volume_db_user},
		$self->config->{volume_db_pass},
		{RaiseError => 1, AutoCommit => 1, FetchHashKeyName => 'NAME_lc'});

	# sqlite setup...
	$dbh->do(q{PRAGMA journal_mode = WAL});
	$dbh->do(q{PRAGMA wal_autocheckpoint = 128}); # 512K, not 4MB
	$dbh->do(q{PRAGMA foreign_keys = ON});

	# we use a blob for the path since we can't guarantee encoding.
	# st_dev is no longer checked, changes on reboot
	$dbh->do(<<QUERY);
CREATE TABLE IF NOT EXISTS volumes (
  path      BLOB      NOT NULL PRIMARY KEY,
  st_dev    INTEGER   NOT NULL,
  st_ino    INTEGER   NOT NULL,
  st_mtime  INTEGER   NOT NULL,
  st_size   INTEGER   NOT NULL,

  vol_lufs  REAL      NULL,
  vol_err   TEXT      NULL,

  CONSTRAINT volume_or_err CHECK ((vol_lufs IS NULL) != (vol_err IS NULL))
)
QUERY

	return $dbh;
}

package main;
MPVVolNorm::Server->run_if_script;
