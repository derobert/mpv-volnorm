What Is It?
===========

Unfortunately, some videos are louder than others. And video formats do
not, generally, have the equivalent of ReplayGain as found in music
files. You could use extended attributes to store it, but NFS doesn't
support that. You can use extra metadata files, but that gets messy and
annoying (but was my previous solution).

mpv-volnorm is a non-annoying solution.

mpv-volnorm has MPV make a simple HTTP request to your file server (or
any other computer) to get the volume. The server will return a cached
value if it has one; else it will tell the client it will have to
compute it. In that case, the client will spit out a warning about the
delay, and ask the server to compute it.

You set up one server, and all the playback clients talk to it. The
cached computed loudnesses are shared between all clients, so you only
have to incur the delay once.


Installation
============

There are two parts of mpv-volnorm: the client (a Lua script which is
run by mpv) and the server (a Perl script that implements a simple web
service for computing, caching, and returning the volume).

Client Installation
-------------------

Make sure lua-socket is installed. Symlink or copy the
`mpv-volnorm-client.lua` file into your MPV scripts directory. This
script has been tested with mpv 0.27 and 0.29.


Server Installation
-------------------

Make sure libwebapplication-simple-perl, libipc-run3-perl, libjson-perl,
libxml-treepp-perl, libdbd-sqlite3-perl, starlet, and bs1770gain are
installed.

Add a user for the server, e.g.,

    adduser --system --group mpv-volnorm-server

The cached volumes are written to a SQLite database in the user's home
directory.

For systemd, a service file is provided. To use it, copy
mpv-volnorm-server.service to `/etc/systemd/system/` and copy
mpv-volnorm-server.psgi to `/usr/local/share/`. Then `systemctl enable
mpv-volnorm-server.service` to make it start on boot and `systemctl
start mpv-volnorm-server.service` to start it now.

Make sure it isn't accessible from outside your firewall; while it's
hopefully secure against anything terrible (and runs as an unprivileged
user), computing volumes can be CPU-intensive so it's a potential denial
of service attack (and of course privacy leak).

Note that it's a (simple) web service, using Plack. So you can deploy it
in any of the normal ways you can deploy Plack apps, including even as a
CGI.

You'll have to arrange permissions so that the server can read your
media files. It does not need or want write permission.


Configuration
=============

Client
------

In your mpv config directory (e.g., `~/.config/mpv` or `~/.mpv`),
create a `script-opts` subdirectory if you don't already have one.
Inside there, edit `mpv-volnorm-client.conf`. There format is simple
key=value pairs, there are three keys:

 - `baseurl` — Where to find the server API. Something like
   `http://your-server-name:34167/`. The port number is configured in
   the systemd service (or however you run the server). The trailing
   slash is usually required. You could use an IP address instead of a
   server name, of course.

 - `basepath` — Where your media files are, *on the client*. This is
   is only used client-side to strip that portion of the path off (it
   sends a relative path to the server). Also, files outside of the
   basepath will be ignored by mpv-volnorm.

 - `target_vol` — How loud you want your files to all be. The default is
   the EBU-recommended -23 LUFS. ATSC recommends -24. This is again a
   client setting, so each client can be different. The volume is
   applied with the simple volume filter — so increasing the volume may
   lead to clipping. Thus pushing this higher is not recommended. 

(Tip: if you run `mpv -v`, the path will be given there if you don't
already have one. Try something like `mpv -v /dev/null |& grep '\.conf
not found'`)

(Compatability note: mpv 0.27 looked under `lua-settings` instead of
`script-opts`; mpv 0.29 looks under both, but documets `script-opts`.)


Server
------

The psgi file has a few default config entries that, well, honestly I
haven't figured out how to override without editing the psgi file ☹.
So... err... edit the file. The keys are:

 - `volume_db_dsn` — The DBI DSN for the volume database. Only SQLite
   has ever been tested, though you could probably use something else if
   you comment out the SQLite pragmas and table creation in
   `_build_dbh`. Probably more usefully, you could change where the
   database is stored.

 - `volume_db_user`, `volume_db_pass` — Username and password for the
   database. Not used by SQLite, so left blank.

 - `media_path` — The path the media is at *on the server*. Defaults to
   `/srv/videos`. Used along with the relative path sent by the client
   to find the media files.


Caching Notes
=============

Pre-caching
-----------

You can pre-cache loudness data for all your media files using
`cli-precacher`. If run without arguments, it will look for your client
configuration and scan all the video files in the basepath. You can also
pass it a list of directories on the command line, then it will only
scan those directories (note that they have to be under the basepath).

It also takes some options

 - `--baseurl`, `--basepath` — Override the value read from the
   configuration file.

 - `--parallel N` — how many requests to run in parallel. Useful if your
   server machine has multiple cores. Defaults to 3.

 - `--extensions REGEXP` — regexp to match file names against to match
   video files. Default is a bunch of file extensions.

Note the precacher needs some more Perl libraries; probably just
libhttp-async-perl, libconfig-simple-perl, and liblist-moreutils-perl.
Everything else should already be installed.

Avoiding Staleness
------------------

The server tries to avoid returning stale results by storing the file
device, inode number, modification time, and size along with the name. A
cache entry is only considered fresh (and thus used) if those match.
Mainly I mention this as somewhere to start if for some reason your
cache is expiring unexpectedly.

Currently, no pruning of cached data for deleted/renamed/etc. files is
implemented. The cache is so much smaller than even a trivial video file
that I haven't worried about it yet, but it should be easy enough to do
(and of course pull requests welcome).
