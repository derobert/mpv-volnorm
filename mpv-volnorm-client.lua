local http = require("socket.http")
local utils = require("mp.utils")
local opt = require("mp.options");

local options = {
	baseurl = 'http://localhost:34167/',
	basepath = '/PLEASE/CONFIGURE/ME/',
	target_vol = -23.0, -- EBU -23, ATSC -24, ReplayGain -18
};
read_options(options, "mpv-volnorm-client")

function on_load()
	local path = utils.join_path(utils.getcwd(), mp.get_property("path"))
	if options['basepath'] ~= string.sub(path, 1, string.len(options['basepath'])) then
		mp.msg.log('v', 'File not under basepath; skipping mpv-volnorm')
		return
	else
		path = string.sub(path, string.len(options['basepath'])+1, -1)
	end

	-- url encode
	path = string.gsub(path, '[^-a-zA-Z0-9._/]', function(c)
		return string.format("%%%02x", string.byte(c))
	end)

	local body, code = http.request(options['baseurl'] .. 'volume/fast/' .. path)
	if 204 == code then
		mp.msg.log('warn', 'Volume not cached; computing (will take a bit)')
		body, code = http.request(options['baseurl'] .. 'volume/slow/' .. path)
	end

	if 200 ~= code then
		mp.msg.log('error', 'Got HTTP error back from volnorm server')
		return
	end

	json = utils.parse_json(body)
	if nil ~= json['err'] then
		mp.msg.log('error', 'Error from server: ' .. json['err'])
		return
	end

	local adj = options['target_vol'] - json['lufs']

	mp.msg.log('info', 'Applying volume adjustment of ' .. adj .. 'dB')
	mp.command('no-osd af add @mpv-volnorm:lavfi-volume='..adj..'dB')
end

mp.add_hook("on_load", 50, on_load)
